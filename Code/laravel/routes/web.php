<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
/*
    return view('welcome');
*/
	return view('top');
});

Route::get('/c200', function() {return view('c200');});
Route::get('/c201', 'RegCustomerController@c201');
Route::get('/c202', 'RegCustomerController@Regist');
Route::get('/c203', function() {return view('c203');});
Route::get('/c204', function() {return view('c204');});

Route::get('/c000', function() {return view('c000');});

Route::get('/c100', function() {return view('c100');});
Route::get('/c101', function() {return view('c101');});
Route::get('/c102', function() {return view('c102');});
Route::get('/c103', function() {return view('c103');});
Route::get('/c111', function() {return view('c111');});
Route::get('/c112', function() {return view('c112');});
Route::get('/c121', function() {return view('c121');});
Route::get('/c122', function() {return view('c122');});
Route::get('/c181', function() {return view('c181');});
Route::get('/c182', function() {return view('c182');});
Route::get('/c191', function() {return view('c191');});
Route::get('/c192', function() {return view('c192');});
Route::get('/c193', function() {return view('c193');});

